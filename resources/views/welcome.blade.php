<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <div class="wrapper">
            <div class="top-part">
                <nav class="row align-items-center page-navigation">
                    <a class="col-sm-2 align-middle page-navigation__logo"><img src="img/LOGO.png"></a>
                    <a class="col-sm-1 align-middle page-navigation__home">Home</a>
                    <a class="col-sm-1 page-navigation__about">About</a>
                    <a class="col-sm-1 page-navigation__services">Services</a>
                    <a class="col-sm-1 page-navigation__portfolio">Portfolio</a>
                    <a class="col-sm-1 page-navigation__shop">Shop</a>
                    <a class="col-sm-1 page-navigation__contact">Contact</a>
                    <a class="col-sm-2 page-navigation__pages">Pages</a>
                    <div class="col-sm-2 page-navigation__contact-us">
                        <a href="#"><img src="img/fb_icon.png"></a>
                        <a href="#"><img src="img/tw_icon.png"></a>
                        <a href="#"> <img src="img/g_icon.png"></a>
                    </div>
                </nav>
                <div class="row page-title">
                    <span class="col-sm-12 page-title__about-us">
                        the best services
                    </span>
                    <span class="col-sm-12 page-title__more-info">
                        We provide the best services in the world. We provide the best services in the world.<br> We provide the best services in the world.
                    </span>
                    <button class="col-sm-1 page-title__btn-hire-us">
                        hire us
                    </button>
                </div>
            </div>
            <div class="row title-information">
                <div class="col-sm-6 title-information__write-pen">
                    <img src="img/write.png">
                </div>
                <div class="col-sm-6 title-information__info">
                    <span class="a-little-about-us">
                        a little about us<br>
                    </span>
                    <div class="basic-information">
                        We are a company based in India with the motto "Diqitalally<br>
                        Designed India". We are a company based in India with the motto<br>
                        "Digitalally Designed India". We are a company based in India<br>
                        with the motto "Digitalally Designed India".
                        <br>
                        <br>
                        We are a company based in India with the motto "Diqitalally<br>
                        Designed India". We are a company based in India with the<br> motto
                        "Digitalally Designed India". We are a company based in<br> India
                        with the motto "Digitalally Designed India". We are<br> a company based in India
                        with the motto "Digitalally Designed India".
                    </div>
                    <button class="read-more">
                        read more details
                    </button>
                </div>
            </div>
            <div class="row align-items-center our-services">
                <div class="col-sm-12 our-services__title">
                  <span>
                    our services
                  </span>
                </div>
            </div>
            <div class="row web-development">
                <div class="col-sm-4 web-development__content">
                    <img src="img/dev.png">
                    <div class="web-development__title">
                        <span class="web-title">
                        web development<br>
                        </span>
                        <div class="description">
                            we provide the best world class web<br> development service We provide the best world<br>
                            class web development service
                        </div>
                        <button class="more-details">
                            more details
                        </button>
                    </div>
                </div>
                <div class="col-sm-4 web-development__content">
                    <img src="img/dev.png">
                    <div class="web-development__title">
                        <span class="web-title">
                        web development<br>
                        </span>
                        <div class="description">
                            we provide the best world class web<br> development service We provide the best world<br>
                            class web development service
                        </div>
                        <button class="more-details">
                            more details
                        </button>
                    </div>
                </div>
                <div class="col-sm-4 web-development__content">
                    <img src="img/dev.png">
                    <div class="web-development__title">
                        <span class="web-title">
                        web development<br>
                        </span>
                        <div class="description">
                            we provide the best world class web<br> development service We provide the best world<br>
                            class web development service
                        </div>
                        <button class="more-details">
                            more details
                        </button>
                    </div>
                </div>
            </div>
            <div class="row align-items-center our-clients">
                <div class="col-sm-12 our-clients__title">
                  <span>
                    clients
                  </span>
                </div>
            </div>
            <div class="row logo-clients">
                <div class="col-sm-3">
                    <img src="img/adobe.png">
                </div>
                <div class="col-sm-3">
                    <img src="img/hcl.png">
                </div>
                <div class="col-sm-3">
                    <img src="img/infosys.png">
                </div>
                <div class="col-sm-3">
                    <img src="img/invision.png">
                </div>
                <div class="col-sm-3 bottom-row__cisco">
                    <img src="img/cisco-logo.png">
                </div>
                <div class="col-sm-3 bottom-row__be">
                    <img src="img/behance.png">
                </div>
                <div class="col-sm-3 bottom-row__canva">
                    <img src="img/Canva-Logo.png">
                </div>
                <div class="col-sm-3 bottom-row__inst">
                    <img src="img/inst.png">
                </div>
            </div>
            <div class="row questions-block">
                <span class="col-sm-12 questions-block__title">
                    Any questions | Get a quote
                </span>
                <div class="col-sm-6 questions-block__info">
                    <span>
                        if you have any questions or queries regarding our services or pricing don`t worry<br>
                        Our experts will call you.
                    </span>
                </div>
                <div class="col-sm-6 questions-block__callback-form">
                    <form>
                        <input type="text" class="user-name" placeholder="Your name"/>
                        <input type="number" class="user-number" placeholder="Your number"/>
                        <input type="submit" class="user-submit" value="Get a call"/>
                    </form>
                </div>
            </div>
            <div class="row footer">
                <div class="col-sm-2">
                    <div class="paddings">
                        <span>product</span>
                    </div>
                    <div class="paddings">
                        <a href="#">Cost & Governance</a>
                    </div>
                    <div class="paddings">
                        <a href="#">Security & Complaince</a>
                    </div>
                    <div class="paddings">
                        <a href="#">Product N</a>
                    </div>
                    <div class="paddings">
                        <a href="#">Pricing & Plans</a>
                    </div>

                </div>
                <div class="col-sm-2">
                    <div class="paddings">
                        <span>learn</span>
                    </div>
                    <div class="paddings">
                        <a href="#">Articles</a>
                    </div>
                    <div class="paddings">
                        <a href="#">Register a demo</a>
                    </div>
                    <div class="paddings">
                        <a href="#">Webinars</a>
                    </div>
                    <div class="paddings">
                        <a href="#">FAQs</a>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="paddings">
                        <span>company</span>
                    </div>
                    <div class="paddings">
                        <a href="#">About</a>
                    </div>
                    <div class="paddings">
                        <a href="#">Careers</a>
                    </div>
                    <div class="paddings">
                        <a href="#">Partners</a>
                    </div>
                    <div class="paddings">
                        <a href="#">Contact</a>
                    </div>
                </div>
                <div class="col-sm-6 footer__subscribe">
                    <form>
                        <input type="text" class="your-email" placeholder="Your email">
                        <input type="submit" class="btn__subscribe" value="Subscribe">
                    </form>
                    <a href="#"><img src="img/fb.png"></a>
                    <a href="#"><img src="img/tw.png"></a>
                    <a href="#"><img src="img/g.png"></a>
                    <a href="#"><img src="img/dri.png"></a>
                    <a href="#"><img src="img/behance-icon.png"></a>
                    <a href="#"><img src="img/insta.png"></a>
                </div>
            </div>
        </div>
    </body>
</html>
